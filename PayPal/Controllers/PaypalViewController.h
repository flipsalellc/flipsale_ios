//
//  PaypalViewController.h

//
//  Created by Imma Web Pvt Ltd on 09/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol payPalDelegate <NSObject>

@required

/**
 When the paypal link is saved then this method is going to be called as a success response

 @param paypalLink Paypal link with the string.
 */
-(void)PaypalLinkIsSaved:(NSString *)paypalLink;
@end

@interface PaypalViewController : UIViewController

@property(nonatomic,strong)NSString *paypalLink;
@property(nonatomic,weak)id<payPalDelegate>paypalCallback;
@end
