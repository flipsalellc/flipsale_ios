//
//  SubcategoryFilterTableViewCell.h
//  CollegeStax
//
//  Created by 3Embed on 22/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCategory.h"

@interface SubcategoryFilterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *subcategoryImage;
@property (weak, nonatomic) IBOutlet UIImageView *tickMark;

@property (weak, nonatomic) IBOutlet UILabel *subcategoryName;

-(void)setSubcategoryFor :(SubCategory *)subcategory selectedSubcategory :(NSString *)selectedSub;

@end
