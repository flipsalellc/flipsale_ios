//
//  PostsCollectionViewCell.h
//  Tac Traderz
//
//  Created by 3Embed on 16/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *postImageView;

/**
 Set Products on homescreen.
 
 @param product productModel.
 */
-(void)setProducts:(ProductDetails *)product;
@end
