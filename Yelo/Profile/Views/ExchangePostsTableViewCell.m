//
//  ExchangePostsTableViewCell.m
//  Tac Traderz
//
//  Created by 3Embed on 31/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "ExchangePostsTableViewCell.h"

@implementation ExchangePostsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setExchangedPostsFor :(Exchanged *)exchanged
{
    [self.postImage sd_setImageWithURL:[NSURL URLWithString:exchanged.mainUrl] placeholderImage:[UIImage imageNamed:@""]];
    [self.userPostImage sd_setImageWithURL:[NSURL URLWithString:exchanged.thumbnailImageUrl] placeholderImage:[UIImage imageNamed:@""]];
    
    [self.exchangedPostImage sd_setImageWithURL:[NSURL URLWithString:exchanged.swapThumbnailImageUrl] placeholderImage:[UIImage imageNamed:@""]];
    
    self.exchangedDescription.text = [NSString stringWithFormat:@"You have exchanged your %@ for %@.",exchanged.productName, exchanged.swapProductName];
    self.timeStampLabel.text = [Helper convertEpochToNormalTimeInshort:exchanged.acceptedTime];
    
}

@end
