//
//  MakeOfferViewController.h

//
//  Created by Rahul Sharma on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CouchbaseLite/CouchbaseLite.h>
#import "CBObjects.h"
#import "CouchbaseEvents.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MBProgressHUD.h"
#import "ContacDataBase.h"
#import "ProductDetails.h"


@class MakeOfferViewController;



@interface MakeOfferViewController : UIViewController <CouchBaseEventsDelegate>

@property (nonatomic)BOOL fromSearchScreen ;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productArea;
@property (strong, nonatomic) IBOutlet UILabel *ProductDistance;
@property (strong, nonatomic) IBOutlet UILabel *actualPrice;
@property (strong, nonatomic) IBOutlet UITextField *OfferpriceTextField;
@property (strong, nonatomic) IBOutlet UIButton *makeOfferOutlet;
- (IBAction)makeOfferButtonAction:(id)sender;
@property (strong, nonatomic) ProductDetails *product;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *viewbuttomConstrain;
@property (strong, nonatomic) CBLQueryEnumerator *result;
- (IBAction)backToProductAction:(id)sender;
-(void)createObjectForMakeOfferwithData:(ProductDetails *)dataDict withPrice:(NSString *)price andType:(NSString *)type withAPICall:(BOOL)isAPICall;
@end

