//
//  ValueViewController.h
//  CollegeStax
//
//  Created by 3Embed on 18/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Filter.h"

typedef void (^callBackForFilterFields)(NSString *value,NSUInteger integerValue);

@interface ValueViewController : UIViewController


@property (nonatomic,strong)NSString *previousValue;
@property (nonatomic,strong)Filter *filter;
@property(copy,nonatomic)callBackForFilterFields valueCallBack;

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (weak, nonatomic) IBOutlet UIView *textBoxView;


@property (weak, nonatomic) IBOutlet UITextField *textBoxTextField;

- (IBAction)tapGestureAction:(id)sender;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGestureOutlet;


- (IBAction)DoneButtonAction:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *doneButtonOutlet;

@property (weak, nonatomic) IBOutlet UIButton *backButtonOulet;

- (IBAction)backButtonAction:(id)sender;

- (IBAction)textFieldEditingChangedAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *doneButtonBottomConstraint;


@end
