//
//  CategoryCollectionViewCell.m
//  CollegeStax
//
//  Created by Rahul Sharma on 18/04/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import "CategoryCollectionViewCell.h"

@implementation CategoryCollectionViewCell

-(void)awakeFromNib
{
    [super awakeFromNib];
}


-(void)setCategories :(Category *)category forIndexPath:(NSIndexPath *)indexPath
{
   [self.categoryImage setImageWithURL:[NSURL URLWithString:category.activeimage] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.categoryName.text = [category.name capitalizedString];
}


@end
