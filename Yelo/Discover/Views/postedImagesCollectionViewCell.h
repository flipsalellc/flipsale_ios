//
//  postedImagesCollectionViewCell.h

//
//  Created by Rahul Sharma on 4/28/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//


@interface postedImagesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *postedImageViewOutlet;

@end
