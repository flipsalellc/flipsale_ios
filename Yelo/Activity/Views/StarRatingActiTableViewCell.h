//
//  StarRatingActiTableViewCell.h

//
//  Created by Imma Web Pvt Ltd on 16/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StarRatingActiTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dicriptionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sellerPic;
@property (weak, nonatomic) IBOutlet UIButton *productNameAction;
@property (weak, nonatomic) IBOutlet UIButton *rateUserAction;
@property (weak, nonatomic) IBOutlet UIButton *buyerNameAction;

@end
