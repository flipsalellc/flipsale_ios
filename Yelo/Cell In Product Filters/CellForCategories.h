//
//  CellForCategories.h

//
//  Created by Rahul Sharma on 02/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^sortByCategory)(NSMutableString *categories, NSURL *imageUrl );
@interface CellForCategories : UITableViewCell <UICollectionViewDelegate , UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic,copy)sortByCategory callBackFilter;
@property (nonatomic) NSMutableArray *arrayOfDefaults,*arrayOfCategoryList;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionviewCategory;

@end
