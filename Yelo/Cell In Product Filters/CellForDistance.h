//
//  CellForDistance.h

//
//  Created by Rahul Sharma on 12/01/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"

typedef void (^distanceSlider)(int distanceValue , NSInteger leadingConstraint);
@interface CellForDistance : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *label;

@property (strong, nonatomic) IBOutlet UISlider *sliderForDistance;

@property (nonatomic) NSInteger leadingConstraint ;

- (IBAction)sliderValueChangedAction:(id)sender;

@property(nonatomic,copy)distanceSlider callBackForDistance;

-(void)setValuesForSlider :(int)distanceValue andLeadingConstraint :(NSInteger)leadingConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *labelLeadingConstraint;
@property (strong, nonatomic) IBOutlet UIView *viewForMovingLabel;


@end
