//
//  SwapSuggessionViewController.h
//  Tac Traderz
//
//  Created by 3Embed on 07/05/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface SwapSuggessionViewController : UIViewController

@property(nonatomic,strong)Listings *listing;
@property(nonatomic,strong)PostListingsViewController *refrenceVC;
@property (weak, nonatomic) IBOutlet UITableView *suggessionTableView;

@property (weak, nonatomic) IBOutlet UICollectionView *swapListCollectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintOfCollectionView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarOutlet;

- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)saveButtonAction:(id)sender;
- (IBAction)removeSwapPostButtonAction:(id)sender;


@end
