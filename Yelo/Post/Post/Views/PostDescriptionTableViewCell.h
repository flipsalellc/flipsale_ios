//
//  PostDescriptionTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface PostDescriptionTableViewCell : UITableViewCell <UITextViewDelegate>

@property (nonatomic,strong) Listings *listing ;
@property (nonatomic, strong)PostListingsViewController *refrenceVC ;
@property (weak, nonatomic) IBOutlet AlignmentTextView *textViewOutletForDescrption;

@end
