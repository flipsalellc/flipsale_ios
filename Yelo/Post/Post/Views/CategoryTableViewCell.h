//
//  CategoryTableViewCell.h
//  Vendu
//
//  Created by Rahul Sharma on 19/03/18.
//  Copyright © 2018 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostListingsViewController.h"

@interface CategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet LabelAlignmentOpposite *value;

@property (nonatomic,strong) Listings *listing ;
@property (nonatomic,strong) PostListingsViewController *referenceVC;

-(void)setTitle:(NSString *)title andValue :(NSString *)value;
- (IBAction)categoryButtonAction:(id)sender;


@end
