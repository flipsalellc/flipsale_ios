//
//  BannerAdsTableViewCell.h
//  ohsello
//
//  Created by Rahul Sharma on 11/10/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerAdsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *bannerAdsView;

@end
